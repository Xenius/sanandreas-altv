# GTA San Andreas map alt:V EARLY ACCESS

This is original GTA:SA map, not Definitive Edition.

# Install
1. Download or clone repo
2. Copy files to resources
3. Add 'sa_gen', 'sa_ls', 'sa_lv', 'sa_sf' to server.cfg (This step is important, if you start resource after server is started map will never load)

Optional: Expand world boundaries (start 'expand' resource)

# Notes
This version contains map bugs, it's very early-access. My plan is to fix all of them and I wan't to add HD textures. Disabling default GTA:V map is planned too.
Didn't work on streaming, FPS drops are possible
If you want, you can separate every directory. Map files and models are located in main folder / stream / map_part.rpf (.rpf is just a directory name)
sa_gen contains San Andreas misc objects, barriers etc.
sa_lv contains Las Venturas
sa_ls contains Los Santos
sa_sf contains San Fierro
Map is located near Vespucci Beach.

# Known issues
- Traffic lights are now working + no collision
- Many objects are blinking because night objects are not separated
- In some places are invisible collisions
- Many collisionless object
- Electrical wires are weird
- Water is not fixed
- SF can't be accessed because of map limitations

# Screenshots
https://i.imgur.com/HrImAxt.jpeg

https://i.imgur.com/pJw54rP.jpeg

https://i.imgur.com/IkQ1miU.jpeg

https://i.imgur.com/ynvdE0y.jpeg

https://i.imgur.com/BnkRqCZ.jpeg

https://i.imgur.com/riFdcHZ.jpeg

https://i.imgur.com/JyoHm8b.jpeg

https://i.imgur.com/EuQNNNK.jpeg
